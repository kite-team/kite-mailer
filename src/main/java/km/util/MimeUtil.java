package km.util;

import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.dom.MessageServiceFactory;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.field.address.AddressBuilder;
import org.apache.james.mime4j.field.address.ParseException;
import org.apache.james.mime4j.message.DefaultMessageWriter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class MimeUtil {
  
  private MessageServiceFactory msgFactory;
  
  public MimeUtil(){
    try{
      msgFactory = MessageServiceFactory.newInstance();
    }
    catch( MimeException e ){
      throw new RuntimeException(e);
    }
  }
  
  public Message parseMessage(String testMail) {
    try{
      return msgFactory.newMessageBuilder().
        parseMessage(new ByteArrayInputStream(testMail.getBytes()));
    }
    catch( Exception e ){
      throw new RuntimeException(e);
    }
  }

  public Message parseMessage(InputStream is) {
    try{
      return msgFactory.newMessageBuilder().parseMessage(is);
    }
    catch( Exception e ){
      throw new RuntimeException(e);
    }
  }

  public void writeMessage(Message msg, OutputStream os){
    DefaultMessageWriter writer = new DefaultMessageWriter();
    try{
      writer.writeMessage(msg, os);
    }
    catch( IOException e ){
      throw new RuntimeException(e);
    }
  }
  
  public String writeMessage(Message msg){
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    writeMessage(msg, baos);
    return baos.toString();
  }
  
  public void removeHeaderFields(Message msg, List<String> names){
    names.forEach(name -> msg.getHeader().removeFields(name));
  }

  public Mailbox parseMailbox(String address) {
    try {
      return AddressBuilder.DEFAULT.parseMailbox(address);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }
}
