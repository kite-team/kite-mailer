
// passed through the TF_VAR_ env variables from the build file (which gets
// them from java system properties)
// On dev machines, I usually set this stuff in my ~/.gradle/gradle.properties
// CI usually passes through to build on command line via -D option
variable "dns_name" { }
variable "forward_address" { }
variable "source_local_name" { }


// set by the build script, this is how TF knows where the build is keeping
// the lambda archive files
variable "lambda_gen_dir" { }

variable "kitemailer_region" {
  default = "us-east-1"
}

variable "kitemailer_receive_mail_lambda_public_name" {
  default = "kitemailer-receive-mail-public-lambda"
}


// edit this or set the variable through the build script after you've
// created the domain record through the AWS SES console
variable "kitemailer_ses_domain_verification" {
  default = ""
}
