### Introduction 

This project implements an SMTP email forwarder using AWS SES and Lambda.  
It can receive an email of the form "something-mail@yourdomain.com" and 
forwards it to any other email address (configurable.)

The project is minimally functional - the intent is to serve as an 
example / tutorial of the 
[Teregrin](https://bitbucket.org/teregrinteam/teregrin)
[Terraform](https://www.terraform.io/) 
wrapper and implementation of an AWS Java Lambda using the 
[ALF4J](https://bitbucket.org/shorn/alf4j) 
library.

Contents

[TOC]

### Platforms

This project works on Windows and Linux (and should work on OS X, but I haven't
tested it).

### Setup prerequisites
#### Prerequisites to running the build
- JDK 1.8+

####Prerequisites to creating AWS infrastructure
- S3 bucket to store the tfstate
- SES email domain

Beware that this project will replace any active SES receipt rule that you have.
Other than that, all resources are self-contained (it doesn't drop security 
groups in your default VPC or anything like that).  I'd recommend running it 
in a separate account from your production infrastrcture though.

### Building the code

Assuming you've cloned the repo to the path `/tmp/kitemailer/src-kitemailer` - 
when building and running Terraform tasks, the build will generate files to the
path `/tmp/kitemailer/gen-kitemailer`.

Assuming the JDK is on your path, just `cd` into the src-kitemailer directory 
and execute `./gradlew build` - this will download all libraries and other
necessities (including the Gradle and Terraform binaries) and then build the 
Java lambda.

### Creating the AWS infrastructure

#### Configuration 

Before running any Terraform tasks to work with your AWS infrastructure, you 
must configure:

- credentials for connecting to AWS and reading/creating resources
- an S3 bucket to store the Terraform state file
- details of the actual email forwarding

Create the following properties in your ~/.gradle/gradle.properties
```
# these two tell the build how to access AWS
systemProp.kitemailer_dev_access_key=AKIABLAHBLAHBLAH
systemProp.kitemailer_dev_secret_key=<secret>

# pre-existing S3 bucket, that the AWS credentials above must be able to access
systemProp.kitemailer_dev_tf_state_bucket=my-tfstate-bucket

# this defines domain we're handling mail for
systemProp.kitemailer_dev_dns_name=mydomain.com

# this is the address we will forward mail to 
systemProp.kitemailer_dev_forward_address=me@gmail.com

# this defines the "From" address that mail will be given
systemProp.kitemailer_dev_source_local_name=kitemailer

# copy this from the SES Email domain verifaction section of your AWS SES 
# console - the build will work without this, but AWS won't accept mail
systemProp.kitemailer_dev_domain_verification=BLAHBLAHBLAH
```

#### Creating infrastructure

- first run `./gradlew devRemoteConfig` to "initialise" the terraform local 
`tfstate` file and "push" the empty file to the remote storage (S3 bucket) 
  - you will find the local state file cache at 
`/tmp/kitemailer/gen-kitemailer/.terraform/terraform.tfstate` 
- then run `./gradlew devPlan` to "plan" the creation of your infrastructure - 
this will report that it is going to create a number of AWS resources
- run `./gradlew devApply` to create the infrastructure - this will keep a 
recored of the all the resources created in the tfstate file and will "push"
that file to your S3 bucket

#### Updating infrastructure on an initialised machine
- make sure your source code is up to date (`git pull` etc.)
- run `./gradlew devPlan` - Terraform will first refresh the local tfstate from 
remote store, then analyse all resources it knows about to see if any changes
need to be made to make the AWS infrastructure conform to the source code.
- run `./gradlew devApply` - Terraform will apply any changes needed

#### Updating infrastructure from an un-initialised machine
- make sure your source code is up to date (`git pull` etc.)
- you just need to run `./gradlew devRemoteConfig` and the build will build 
everything locally, then configure your local tfstate file from remote storage
  - after that, you just follow the update instructions above

### Source Code
#### Structure
- `/` root contains git / gradle / gradle wrapper config and scripts
- `/gradle/` contains the gradle wrapper config and binary, can be ignored
- `/src/main/java/` java source code of the forwarding lambda
- `/src/main/resources/` properties files and other config of the Java code
- `/src/main/terraform/` terraform source code of the AWS resources 
 
#### Important files
- [/build.gradle](/build.gradle) the gradle build file that defines the tasks
- [/src/main/java/km/ReceiveMailLambda.java](/src/main/java/km/ReceiveMailLambda.java) 
the mail processing lambda
