
resource "aws_lambda_permission" "kitemailer-allow-ses-lambda" {
  statement_id = "kitemailer-allow-ses-lambda"
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.kitemailer-receive-mail-public-lambda.arn}"
  principal = "ses.amazonaws.com"
  source_account = "${data.aws_caller_identity.current.account_id}"
}

resource "aws_ses_receipt_rule_set" "kitemailer-ses-receipt-ruleset" {
  rule_set_name = "kitemailer-ses-receipt-ruleset"
}

resource "aws_ses_receipt_rule" "kitemailer-ses-receipt-rule" {
  depends_on = ["aws_lambda_permission.kitemailer-allow-ses-lambda"]
  name = "kitemailer-ses-receipt-rule"
  rule_set_name = "${aws_ses_receipt_rule_set.kitemailer-ses-receipt-ruleset.rule_set_name}"
  enabled = true
  scan_enabled = false

  s3_action {
    bucket_name = "${aws_s3_bucket.kitemailer-ses-receive-s3bucket.bucket}"
    position = 0
  }

  lambda_action {
    function_arn = "${aws_lambda_function.kitemailer-receive-mail-public-lambda.arn}"
    // "event" == "asynchronous"
    invocation_type = "Event"
    position = 1
  }
}

// this will replace any other receipt ruleset that you currently have active
resource "aws_ses_active_receipt_rule_set" "kitemailer-ses-active-receipt-ruleset" {
  rule_set_name = "${aws_ses_receipt_rule_set.kitemailer-ses-receipt-ruleset.rule_set_name}"
}

