package km;

import km.event.ses.SesMailReceiveEvent;
import km.util.MimeUtil;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import org.alf4j.LambdaHandler;
import org.alf4j.log.Log;
import org.alf4j.util.stateful.JsonUtil;
import org.alf4j.util.stateful.S3Util;
import org.alf4j.util.stateful.SesUtil;
import org.alf4j.util.stateless.Check;
import org.apache.commons.codec.Charsets;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.dom.address.MailboxList;
import org.apache.james.mime4j.stream.RawField;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import static org.alf4j.util.stateless.ObjectUtil.infoLogExecutionTime;
import static org.alf4j.util.stateless.StringUtil.*;
import static org.alf4j.util.stateless.StringUtil.areEqual;
import static org.alf4j.util.stateless.StringUtil.format;

/**
 * Expects some environment variables to be configured,
 * see {@link #ReceiveMailLambda()}.
 */
public class ReceiveMailLambda
extends LambdaHandler<SesMailReceiveEvent, String> {
  // "x-*" style custom headers are deprecated 
  // https://tools.ietf.org/html/rfc6648
  private static final String KM_SENT_HEADER = "x-kitemailer-outgoing";
  
  /** list of well-known fields that will be stripped from
   * forwarded mail.
   * Note that message-id is intentionally not filtered, see:
   * http://stackoverflow.com/a/7591704/924597
   */
  private static List<String> FILTERED_FIELDS = Arrays.asList(
    // if accidentally leave this, can end up with a loop!
    "to",
    
    // removed fields that will be set as part of return handling
    "return-path",
    "from",
    "received",
    
    // probably don't need to strip these, other MTAs should deal with
    // them appropriately anyway (delete, overwrite, ignore, etc.)
    "dkim-signature",
    "x-ses-dkim-signature",
    "x-ses-receipt",
    "authentication-results",
    "received-spf",
    "x-received",
    "x-gm-message-state",
    "x-google-dkim-signature"
  );

  // these fields are just util classes
  private Log log = Log.to(this);
  private JsonUtil json = new JsonUtil();
  private S3Util s3 = new S3Util();
  private MimeUtil mime = new MimeUtil();
  private SesUtil ses = new SesUtil();

  // these fields come from environment variables, populated in the ctor
  private String emailS3Bucket;

  /** Quick and easy way to stop *forwarding* mail (in case of mail
   * loops or other problems).
   * Disable the SES receipt rule if you want to stop *accepting* mail.
   */
  private boolean stopForwarding = false;

  /** parsed from sourceAddress env var */
  private Mailbox sourceMailbox;

  /** parse from forwardTarget env var */
  private Mailbox forwardMailbox;

  /** in order to be forwarded, mail must match this pattern */
  private Predicate<String> validMail;

  /** AFAICT - the ctor only gets called once, for each active lambda which
   * runs in a dedicated JVM.
   * So this is where we parse out env variables, do pre-processing, etc.
   * For testing, you would factor out two ctors and do all System.getEnv()
   * stuff in the default ctor and the test would call the other ctor.
   */
  public ReceiveMailLambda(){
    emailS3Bucket = System.getenv("emailS3Bucket");
    Check.hasValue("emailS3Bucket must be set in environment", emailS3Bucket);

    stopForwarding = Boolean.parseBoolean(System.getenv("stopForwarding"));

    sourceMailbox = mime.parseMailbox(System.getenv("sourceAddress"));
    forwardMailbox = mime.parseMailbox(System.getenv("forwardAddress"));

    String validMailRegex = System.getenv("validMailRegex");
    if( isBlank(validMailRegex) ){
      validMailRegex = ".*mail@" + sourceMailbox.getDomain();
    }
    validMail = Pattern.compile(validMailRegex).asPredicate();

    log.info( "ctor(), emailS3Bucket=`%s`, stopForwarding=%s, " +
        "sourceAddress=%s, forwardAddress=%s",
      emailS3Bucket, stopForwarding,
      sourceMailbox.getAddress(), forwardMailbox.getAddress() );
  }

  @Override
  public SesMailReceiveEvent parseInput(InputStream input){
    return json.decode(input, SesMailReceiveEvent.class);
  }
  
  @Override
  public String doWork(SesMailReceiveEvent input, Context context) {
    /* If it's ever shown that SES actually sends multiple emails in a single
    event - this is where the logic would be to do parallel processing.
    Either by constructing multiple instance of this class (that's why
    the util classes are per-instance instead of static) or by using the
    AWS API to invoke ReceiveMailLambda recursively, AWS would then
    spin up more Lambda JVMs to deal with those requests.
    That's the theory anyway, haven't tried it yet :)
    */
    input.getRecords().forEach(this::processSesRecord);
    return "success";
  }

  private void processSesRecord(SesMailReceiveEvent.SesRecord record){
    SesMailReceiveEvent.Mail mail = record.ses.mail;
    log.info("processing messageId %s|subject %s|destinations: %s",
      mail.messageId, mail.commonHeaders.subject , format(mail.destination) );

    // Get the email from S3 ASAP, then work on the Mime4J parsed version
    // because SES truncates headers from the input if > 10KB
    S3ObjectInputStream emailContent = infoLogExecutionTime(
      log, "get email from s3",
      () -> s3.getObject(emailS3Bucket, mail.messageId).getObjectContent()
    );
    
    Message msg = infoLogExecutionTime(
      log, "parse mime message",
      () -> mime.parseMessage(emailContent)
    );
    
    List<String> mailProblems = findProblemsWithMail(msg);
    if( !mailProblems.isEmpty() ){
      mailProblems.forEach( i -> log.info(i) );
      log.info("input of abandoned mail: %s", json.encodePretty(record));
      return;
    }
  
    mime.removeHeaderFields(msg, FILTERED_FIELDS);
    
    msg.setFrom(sourceMailbox);
    msg.setTo(forwardMailbox);
  
    // identify mails sent by this lambda
    msg.getHeader().addField(new RawField(KM_SENT_HEADER, "true"));
    
    if( stopForwarding ){
      log.info("email NOT forwarded because stopForwarding is set");
      return;
    }

    // This could be improved, writeMessage() is producing a string,
    // would be better to work more directly with a stream/buffer.
    // But keep the two steps separate - don't conflate Mime4J and SES.
    ByteBuffer emailContentBuffer = infoLogExecutionTime(
      log, "encode mime message",
      () -> Charsets.UTF_8.encode(mime.writeMessage(msg))
    );
    infoLogExecutionTime( log, "send raw email", 
      () -> ses.sendRawEmail(emailContentBuffer)
    );
  }
  
  private List<String> findProblemsWithMail(Message mail){
    List<String> problems = new ArrayList<>();

    // try to deal with an accidental send loop.
    // Dubious value: MUA's won't forward it and MTA's do whatever they want
    // (especially beware of MS Exchange)
    if( mail.getHeader().getField(KM_SENT_HEADER) != null ){
      problems.add("mail contains the `sent-by-cm` header");
    }
    
    MailboxList toList = mail.getTo().flatten();
    
    if( toList.isEmpty() ){
      problems.add("mail has no destination");
    }
    else if( toList.size() > 1 ){
      problems.add("mail has multiple destinations");
    }

    String souceLocal = sourceMailbox.getLocalPart();
    // consider factoring this "is addressed to" logic to MimeUtil
    Mailbox destination = toList.get(0);
    if( areEqual(destination.getLocalPart(), souceLocal) ) {
      problems.add("mail is addressed to kitemailer");
    }

    if( !validMail.test(destination.getAddress()) ){
      problems.add("mail to doesn't match valid mail pattern: " + destination);
    }
    
    return problems;
  }
  
}
