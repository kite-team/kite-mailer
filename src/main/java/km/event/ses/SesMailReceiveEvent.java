package km.event.ses;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/* Yes, the fields are declared public.
Just think of it as a struct, the getters and setters are only there
for the JSON parser - yay for code generation.
*/
public class SesMailReceiveEvent {
  
  public  List<SesRecord> records;
  
  /* This is the secret sauce - for whatever reason, AWS delivers the event
   * json under the attribute "Records" with an upper-care R, which doesn't 
   * work well with the javabean standard, so we have to explicitly tell 
   * Jackson that the name will be upper-case.
   */
  @JsonProperty("Records")  
  public List<SesRecord> getRecords(){
    return records;
  }
  
  public void setRecords(List<SesRecord> records){
    this.records = records;
  }
  
  public static class SesRecord {
    public String eventSource;
    public String eventVersion;
    public Ses ses;
  
    public String getEventSource(){
      return eventSource;
    }
  
    public void setEventSource(String eventSource){
      this.eventSource = eventSource;
    }
  
    public String getEventVersion(){
      return eventVersion;
    }
  
    public void setEventVersion(String eventVersion){
      this.eventVersion = eventVersion;
    }
  
    public Ses getSes(){
      return ses;
    }
  
    public void setSes(Ses ses){
      this.ses = ses;
    }
  }
  
  
  public static class Ses {
    public Mail mail;
  
    public Mail getMail(){
      return mail;
    }
  
    public void setMail(Mail mail){
      this.mail = mail;
    }
  } 
  
  public static class Mail {
    public String timestamp;
    public String source;
    public String messageId;
    public List<String> destination;
    public boolean headersTruncated;
    public List<Header> headers;
    public CommonHeaders commonHeaders;
  
    public String getTimestamp(){
      return timestamp;
    }
  
    public void setTimestamp(String timestamp){
      this.timestamp = timestamp;
    }
  
    public String getSource(){
      return source;
    }
  
    public void setSource(String source){
      this.source = source;
    }
  
    public String getMessageId(){
      return messageId;
    }
  
    public void setMessageId(String messageId){
      this.messageId = messageId;
    }
  
    public List<String> getDestination(){
      return destination;
    }
  
    public void setDestination(List<String> destination){
      this.destination = destination;
    }
  
    public boolean isHeadersTruncated(){
      return headersTruncated;
    }
  
    public void setHeadersTruncated(boolean headersTruncated){
      this.headersTruncated = headersTruncated;
    }
  
    public List<Header> getHeaders(){
      return headers;
    }
  
    public void setHeaders(List<Header> headers){
      this.headers = headers;
    }
  
    public CommonHeaders getCommonHeaders(){
      return commonHeaders;
    }
  
    public void setCommonHeaders(CommonHeaders commonHeaders){
      this.commonHeaders = commonHeaders;
    }
  }
  
  public static class CommonHeaders {
    public List<String> from;
    public List<String> to;
    public String returnPath;
    public String messageId;
    public String date;
    public String subject;
  
    public List<String> getFrom(){
      return from;
    }
  
    public void setFrom(List<String> from){
      this.from = from;
    }
  
    public List<String> getTo(){
      return to;
    }
  
    public void setTo(List<String> to){
      this.to = to;
    }
  
    public String getReturnPath(){
      return returnPath;
    }
  
    public void setReturnPath(String returnPath){
      this.returnPath = returnPath;
    }
  
    public String getMessageId(){
      return messageId;
    }
  
    public void setMessageId(String messageId){
      this.messageId = messageId;
    }
  
    public String getDate(){
      return date;
    }
  
    public void setDate(String date){
      this.date = date;
    }
  
    public String getSubject(){
      return subject;
    }
  
    public void setSubject(String subject){
      this.subject = subject;
    }
  }
  
  public static class Header {
    public String name;
    public String value;
  
    public String getName(){
      return name;
    }
  
    public void setName(String name){
      this.name = name;
    }
  
    public String getValue(){
      return value;
    }
  
    public void setValue(String value){
      this.value = value;
    }
  }
  
  public static class Receipt {
    public String timestamp;
    public Long processingTimeMillis;
  
    public Long getProcessingTimeMillis(){
      return processingTimeMillis;
    }
  
    public void setProcessingTimeMillis(Long processingTimeMillis){
      this.processingTimeMillis = processingTimeMillis;
    }
  
    public String getTimestamp(){
      return timestamp;
    }
  
    public void setTimestamp(String timestamp){
      this.timestamp = timestamp;
    }
  }
}

