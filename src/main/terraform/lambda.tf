
// If you want the lambda to run in a VPC (private or public) you will need NAT.
// That's what the "public" in the name is trying to explain.
resource "aws_lambda_function" "kitemailer-receive-mail-public-lambda" {
  function_name = "${var.kitemailer_receive_mail_lambda_public_name}"
  description = "src-kitemailer"
  filename = "${var.lambda_gen_dir}/kite-mailer.jar"
  source_code_hash = "${base64sha256(file("${var.lambda_gen_dir}/kite-mailer.jar"))}"
  handler = "km.ReceiveMailLambda::handleLambdaInvocation"
  role = "${aws_iam_role.kitemailer-receive-mail-lambda-role.arn}"
  runtime = "java8"
  publish = false

  // 192 to give us more metaspace, basic code was taking > 12MB of meta,
  // causing metaspace OOM errors.
  // 128 gives 12 MB of metaspace, 192 gives 20 MB
  memory_size = "192"

  // Worst case assumptions:
  // - 10 secs to boot lambda
  // - 20 secs get mail from S3
  // - 20 secs to send mail via SES
  // Gives 10 seconds of processing time still.
  // I've never seen numbers like those yet.
  // When warm, execution takes 200 - 300 ms (all of that in S3 and SES).
  // When cold, I've observed 4 - 6 sec for boot, 2 - 15 sec for S3 and
  // 2 - 15 sec for SES.
  // I'm guessing a big part of the cold numbers is class loading for S3 and
  // SES (which is really just more "boot" time).
  // Execution time can also be improved by allocating more memory (as per the
  // Lambda documentation.)
  timeout = "60"

  environment{
    variables {
      "emailS3Bucket" = "${aws_s3_bucket.kitemailer-ses-receive-s3bucket.bucket}"
      "forwardAddress" = "${var.forward_address}"
      "sourceAddress" = "${var.source_local_name}@${var.dns_name}"
    }
  }
}

// technically, not necessary because the security group would just end up in
// the default VPC if we didn't specify - but some people delete their default
// VPC, or might be using it for other things and this would just clutter
// up that VPC needlessly.
resource "aws_vpc" "kitemailer-vpc" {
  // set it to whatever you want
  cidr_block = "10.42.0.0/16"
  enable_dns_hostnames = true
  tags {
    Name = "kitemailer-vpc"
    ManagedBy = "src-kitemailer"
  }
}

// Could go in the default VPC - but I don't like that, see above.
resource "aws_security_group" "kitemailer-receive-mail-lambda-sg" {
  name = "kitemailer-receive-mail-lambda-sg"
  tags {
    Name = "kitemailer-receive-mail-lambda-sg"
    ManagedBy = "src-kitemailer"
  }
  description = "Security group for lambda to receive mail"
  vpc_id = "${aws_vpc.kitemailer-vpc.id}"
}

// so the lambda can talk to S3
resource "aws_security_group_rule" "https-receive-mail-lambda-to-internet" {
  type = "egress"
  from_port = 443
  to_port = 443
  protocol = "tcp"
  security_group_id = "${aws_security_group.kitemailer-receive-mail-lambda-sg.id}"
  cidr_blocks = ["0.0.0.0/0"]
}

// this is where the lambda will log to
resource "aws_cloudwatch_log_group" "kitemailer-receive-mail-public-lambda-loggroup" {
  name = "/aws/lambda/${var.kitemailer_receive_mail_lambda_public_name}"
  retention_in_days = 14
}

