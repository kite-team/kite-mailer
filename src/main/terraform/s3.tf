
// this is where the email content lands, as specified by the receipt rule.
resource "aws_s3_bucket" "kitemailer-ses-receive-s3bucket" {
  tags {
    Name = "kitemailer-ses-receive-s3bucket"
    ManagedBy = "src-kitemailer"
  }

  bucket = "kitemailer-ses-receive-bucket"
  acl = "private"

  // dangerous, but this project is jsut an example and this is needed
  // so our destroy command can work in one shot
  force_destroy = true

  policy = <<EOF
{
  "Version":"2012-10-17",
  "Statement": [
  {
    "Sid": "AllowSesPermissionToWrite",
    "Effect": "Allow",
    "Principal": {
      "Service": [
        "ses.amazonaws.com"
      ]
    },
    "Action": [
      "s3:PutObject"
    ],
    "Resource": "arn:aws:s3:::kitemailer-ses-receive-bucket/*",
    "Condition": {
      "StringEquals": {
        "aws:Referer": "${data.aws_caller_identity.current.account_id}"
      }
    }
  }
  ]
}
EOF
}

resource "aws_s3_bucket_object" "kitemailer-ses-receive-s3-owner-object" {
  bucket = "${aws_s3_bucket.kitemailer-ses-receive-s3bucket.bucket}"
  key = "readme.txt"
  content_type = "text/plain"

  content = <<EOF
  This bucket stores the emails received from SES.
  The bucket is managed out of the kite-mailer source code.
EOF
}
