
// In order to use this zone and records, you must configure your DNS
// infrastructure to "delgate" the subdomain (specified by var.dns_name) to
// this zone. After creation of the zone below, go look at the NS record
// in the AWS Route53 console and copy the NS record server names out into
// a new NS record in your own DNS admin tool (route53, DynDNS, etc.)

// Each route53 zone costs .50 USD (not free tier eligible).
// But you don't have to use it - if you have your own DNS admin already, just
// add the verification and MX records manually yourself.
// Or TF has providers for Dyn and other DNS infrastructure, if you're
// feeling adventurous.)
// Comment these resources out or delete the file if you want to handle DNS
// yourself.
resource "aws_route53_zone" "kitemailer-zone" {
  name = "${var.dns_name}"
  comment = "managed by src-kitemailer"
  tags {
    Name = "primary zone - ${var.dns_name}"
    ManagedBy = "src-kitemailer"
  }
}

// The actual creation of the "SES email domain" and domain verification
// is initiated by hand through AWS SES console (the name of the domain
// must match "dns_name" variable).
// There is no Terraform resource for an SES domain that I know of.
// You then copy verification token into your gradle.properties file.
resource "aws_route53_record" "kitemailer-ses-verifiaction-route53-record" {
  zone_id = "${aws_route53_zone.kitemailer-zone.zone_id}"

  name = "_amazonses.${aws_route53_zone.kitemailer-zone.name}"
  type = "TXT"
  // This is the token that verifies to SES that we control this domain.
  // It is not a secret though - given that we're creating it as a publicly
  // available DNS entry for all the world to see.
  records = ["${var.kitemailer_ses_domain_verification}"]
  // short for development / debugging purposes
  ttl = "30"
}

// This is the bit that tells email servers around the world to send our
// mail to SES. The previous manual creation of an SES Domain with
// a name that matches "dns_name" is how SES knows to direct mail to
// our receipt rules.
resource "aws_route53_record" "kitemailer-ses-mx-route53-record" {
  zone_id = "${aws_route53_zone.kitemailer-zone.zone_id}"
  name = "${aws_route53_zone.kitemailer-zone.name}"
  type = "MX"
  records = ["10 inbound-smtp.${data.aws_region.current.name}.amazonaws.com"]
  // short for development / debugging purposes
  ttl = "30"
}
