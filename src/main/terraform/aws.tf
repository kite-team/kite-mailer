provider "aws" {
  region = "${var.kitemailer_region}"
}

data "aws_region" "current" {
  current = true
}

data "aws_iam_account_alias" "current" { }

data "aws_caller_identity" "current" { }

data "aws_availability_zones" "current" { }

